# fedbotrandom.pl Change Log

## 1.2 - 2024-11-14
* Update access token from URL parameter to Authorization header.
* Add visibility option so posts to Snac will be, well, visible.
* Remove recommendation for botsin.space since it's shutting down next month.

## 1.1 - 2023-07-10
* Add support for proxies via LWP (contributed by Sandra Snan)

## 1.0 - 2022-04-03
* BREAKING CHANGE: Moved configuration to a separate file. Now one script can run multiple bots.

## 0.9 - 2022-01-19
* Explicitly chose MIT license and moved to Codeberg.
* Added multiline support.

## Unnumbered - 2018-09-16
* Initial release
* Able to read files, pick a line, and post it.


